# Instructions

All readings are optional, but encouraged.

## Initial checks

### Do you have git installed?
  - yes: continue
  - no: follow these [instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Have you committed with git before?
  - yes: continue
  - no: configure git by giving it your information


        git config --global user.name "YOUR NAME HERE"
        git config --global user.email YOUR@EMAIL.HERE

### Do you have Bitbucket log in?
  - yes: log in, then continue
  - no: [sign up](https://bitbucket.org/account/signup/)

## Get the code

### Fork the project
  - Click the "Fork" button in the sidebar
  ![Create fork](http://lucybain.bitbucket.org/readme-images/create-fork-1.png)

  - When you create your fork, be sure to rename the project to be `YOUR_BITBUCKET_USERNAME.bitbucket.org`
  ![Create fork](http://lucybain.bitbucket.org/readme-images/create-fork.png)

  - Visit the current version of your site by going to YOUR_BITBUCKET_USERNAME.bitbucket.org
  - Optional: read [how to fork in Bitbucket](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)

### Get a local copy of the project
  - Copy the `git clone` command Bitbucket gives you. Be sure to use the `https` option unless you already have ssh keys set up for Bitbucket.

  ![Clone](http://lucybain.bitbucket.org/readme-images/clone.png)

## Make some initial changes

### Make a new header
  - change `index.html`
  - remove the "Hello world" line (line 15) and replace it with this:

        <div class="container">
          <div class="row">
            <div class="navbar-header">
              <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="#" class="navbar-brand"><img src="demo_files/logo-adg.png" height="30px "></a>
            </div>
            <div id="navbar-collapse-1" class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#home">Home</a></li>
                <li><a href="#features">Features</a></li>
                <li><a href="#details">Details</a></li>
                <li><a href="#testimonials">Testimonials</a></li>
              </ul>
            </div>
          </div>
        </div>

  - Let git know about these changes: `git add index.html`
  - Make a commit: `git commit -m "new header"`
    - `-m` stands for "message" and lets you write a note to go along with the commit
    - The message can be whatever you want, but make it clear
    - Optional: read about [good commit messages](http://chris.beams.io/posts/git-commit/)
  - Update the remote (Bitbucket) with your chagnes: `git push origin master`
  - Visit the current version of your site by going to YOUR_BITBUCKET_USERNAME.bitbucket.org

### Add main content
We're going to add some content. Since this is a big piece of work we should do it on a branch.

  - Make a new branch: `git branch new-content`
  - Change to the new-content branch: `git checkout new-content`
  - make changes to `index.html`
  - change `<div class="main-content"></div>` to:

         <div class="main-content">
            <div id="banner" class="banner">
              <div class="container">
                <h2>Design a great Atlassian experience</h2><a href="javascript:;" target="_blank" class="btn btn-download btn-lg">Download</a><br><img src="./demo_files/preview-simple.png" alt="preview" class="preview-img">
              </div>
            </div>
            <div id="features" class="features">
              <header>
                <h3 class="text-center">Responsive multi-layout admin built with AngularJS. </h3>
              </header>
              <div class="container">
                <div class="row">
                  <div class="feature-item col-md-4 text-center"><i class="fa fa-arrows-alt"></i>
                    <h3>Responsive Design</h3>
                    <div class="desc">
                      <p>Auto adjust layout according to screen size. Looks good on desktop, tablet and phone.</p>
                    </div>
                  </div>
                  <div class="feature-item col-md-4 text-center"><i class="fa fa-columns"></i>
                    <h3>Multiple Layouts</h3>
                    <div class="desc">
                      <p>Wide and boxed layout.<br>vertical and horizaon navigation，<br>toggle for fixed top header and sidebar menu.</p>
                    </div>
                  </div>
                  <div class="feature-item col-md-4 text-center"><i class="fa fa-desktop"></i>
                    <h3>Web App</h3>
                    <div class="desc">
                      <p>Single Page Application Built with AngularJS. <br>Fully AJAX powered, never refresh the entire page again. Faster, smoother.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="details" class="details">
              <ul class="list-details list-unstyled">
                <li class="alt">
                  <div class="container">
                    <div class="detail-item clearfix">
                      <div class="detail-desc">
                        <h2>Multiple Layouts</h2>
                        <p>Wide and boxed layout.<br>vertical and horizaon navigation，<br>toggle for fixed top header and sidebar menu.</p>
                      </div>
                      <div class="detail-img"><img src="./demo_files/preview-layouts.png" alt="img"></div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="container">
                    <div class="detail-item clearfix">
                      <div class="detail-desc">
                        <h2>Web App</h2>
                        <p>Single Page Application Built with AngularJS. <br>Fully AJAX powered, never refresh the entire page again.<br>Faster, smoother.</p>
                      </div>
                      <div class="detail-img"><img src="./demo_files/preview-app.png" alt="img"></div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>

  - Commit these changes
    - `git add index.html`
    - `git commit -m "updated content"`
  - Update the remote (Bitbucket) with your chagnes: `git push origin new-content`

### Let's try that one more time..
  - Make sure you're on the `new-content` branch. You can check by using `git branch` and making sure that `new-content` has a `*` next to it.
  - After all the new main-content div (on line 93) add:

          <div id="testimonials" class="testimonials">
            <div id="testimonials-carousel" data-ride="carousel" class="carousel slide">
              <ol class="carousel-indicators">
                <li data-target="#testimonials-carousel" data-slide-to="0" class=""></li>
                <li data-target="#testimonials-carousel" data-slide-to="1" class=""></li>
                <li data-target="#testimonials-carousel" data-slide-to="2" class=""></li>
                <li data-target="#testimonials-carousel" data-slide-to="3" class="active"></li>
                <li data-target="#testimonials-carousel" data-slide-to="4" class=""></li>
              </ol>
              <div class="carousel-inner text-center">
                <div class="item">
                  <p>Great job! It's simply wonderful!</p><small>- Comments from buyer</small>
                </div>
                <div class="item">
                  <p>Very impresive for being the first project here at wrapBootstrap and the very first angularjs theme amazing! Lot of plugins that are useful and not just inflate the package.</p><small>- Comments from buyer</small>
                </div>
                <div class="item">
                  <p>Great Job! First Angular JS Template on wrapBootstrap!</p><small>- Comments from buyer</small>
                </div>
                <div class="item active">
                  <p>Really awesome that you're the first one who creates an AngularJS for wrapBootstrap. I've been looking for one for a long time, and it's really annoying to convert a non AngularJS theme to an AngularJS theme, so thanks for making my life easier.</p><small>- Comments from buyer</small>
                </div>
                <div class="item">
                  <p>Amazing! Exact what I looking for.</p><small>- Comments from buyer</small>
                </div>
              </div>
            </div>
          </div>

  - Commit these changes
    - `git add index.html`
    - `git commit -m "added carousel"`
  - Again, update the remote (Bitbucket) with your chagnes: `git push origin new-content`

## Change code on the remote

### Create a pull request for content changes
Often you want someone else to review your code before you merge it. To do this, you open a "pull request" and ask someone to review your changes. Since pull requests are used so frequently we just call the PRs.

  - Do these steps in your repo on Bitbucket
  ![Create pull request 1](http://lucybain.bitbucket.org/readme-images/create-pr-button.png)
  ![Create pull request 2](http://lucybain.bitbucket.org/readme-images/create-pr.png)

  - Optional: Read about [PRs](https://www.atlassian.com/git/tutorials/making-a-pull-request/)

### Review and merge PR
After you've reviewed your code, you can merge it into master. (Typically you get someone else to review your code, but we'll skip that step in this exercise.)
  ![Merge pull request 1](http://lucybain.bitbucket.org/readme-images/merge-pr-1.png)
  ![Merge pull request 2](http://lucybain.bitbucket.org/readme-images/merge-pr-2.png)

- Visit the current version of your site by going to YOUR_BITBUCKET_USERNAME.bitbucket.org

### Get you pull request changes from the remote
  - Change to the master branch by using `git checkout master`
  - Pull down the changes from the remote: `git pull origin master`

## Fix a merge conflict

### Add footer content in new branch
  - From `master` create and checkout a new branch:
    - `git checkout -b footer`
    - This is a shortcut for: `git branch footer; git checkout footer`
  - make changes to `index.html`
    - on line 122 add:

            <footer class="footer text-center">Copyright © 2014 Atlassian</footer>

  - Commit these changes
    - `git add index.html`
    - `git commit -m "new footer"`
  - **DO NOT PUSH** (we're going to merge locally rather than in a PR on Bitbucket)

### Change footer on master
We're going to make another footer on the master branch. This will force a conflict so you get practice respolving them.

  - `git checkout master`
  - change `index.html`
  - on line 122 add:

        This is not a real footer!

  - Commit these changes
    - `git add index.html`
    - `git commit -m "bad footer"`
  - Again, **DO NOT PUSH**

### Merge footer to master
  - Check that you're on the master branch (again, use `git branch` to see which branch you're on)
  - Merge the `footer` branch to `master` use: `git merge footer`
  - Optional: read about the [merge command](https://www.atlassian.com/git/tutorials/using-branches/git-merge)
  - Git should tell you about the conflict:

        CONFLICT (content): Merge conflict in `index.html`
        Automatic merge failed; fix conflicts and then commit the result.

### Resove merge conflict
  - Look at `index.html`, it should look like this:
  ![Create fork](http://lucybain.bitbucket.org/readme-images/merge-conflict-1.png)
  - Delete lines until it looks like this:
  ![Create fork](http://lucybain.bitbucket.org/readme-images/merge-conflict-2.png)
  - Optional: read more about [merge conflicts](http://stackoverflow.com/a/7589612/863846)
  - commit changes
    - `git add index.html`
    - `git commit -m "fix merge conflict"`

### Update the origin with the correct footer
  - `git push origin master`

## View your updated site
  - Go to your site at YOUR_BITBUCKET_USERNAME.bitbucket.org